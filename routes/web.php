<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//Basic pages
Route::get('/', function () {
    return view('app.others.index');
});
Route::get('/about',function(){
	return view('app.others.about');
});
Route::get('/contact',function(){
	return view('app.others.contact');
});
//Other pages
Route::get('/login','Auth\authController@login');
Route::get('/logout','Auth\authController@logout');
Route::post('/verify-login','Auth\authController@verify_login');
Route::get('/register','Auth\authController@register');
Route::post('/user-registration','Auth\authController@verify_registration');
Route::group(['middleware'=>['auth']], function(){
	//Admin paro
	Route::get('/allusers','app\adminController@view_allusers');

	//Profile Management
	Route::get('/viewprofile','app\profileController@view_profile');
	Route::post('/editprofile','app\profileController@view_profile');
	Route::get('profile/suspend/{id}','app\profileController@suspend');
	Route::get('profile/activate/{id}','app\profileController@activate');
	Route::get('profile/admin/{id}','app\profileController@toadmin' );
	Route::get('/logout', 'Auth\authController@logout');
	//Elections paro
	
	
	Route::get('/electionresults','app\electionController@electionresults');
	Route::get('/electionvote','app\electionController@vote');

	Route::get('elections','app\electionController@index');
	Route::get('election/setup/{id?}','app\electionController@setupElection');
	Route::get('election/view/{id}','app\electionController@viewElection');
	Route::get('election/start/{id}','app\electionController@startElection');
	Route::get('election/conclude/{id}','app\electionController@concludeElection');
	Route::get('election/cancel/{id}','app\electionController@cancelElection');
	Route::get('election/applications/{id}','app\electionController@viewApplications');
	Route::get('election/applications/approve/{applicationId}','app\electionController@approveApplication');
	Route::get('election/applications/decline/{applicationId}','app\electionController@declineApplication');
	Route::get('election/category/delete/{id}','app\electionController@deleteElectionCategory');
	Route::get('election/polls/{id}','app\electionController@polls');
	Route::get('election/polls/vote/{electId}/position/{posId}/contestant/{constId}','app\electionController@castVote');
	Route::post('election/category/setup/{id?}','app\electionController@setupCategory');
	Route::post('election/setup/{id?}','app\electionController@setupElection');
	Route::post('election/apply','app\electionController@sendApplication');


});