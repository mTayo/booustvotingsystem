<?php

namespace App\Http\Controllers\app;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;

class adminController extends Controller
{
    function view_allusers(){
    	$users=User::all();
    	$data=[];
    	$data['user']=$users;
    	return view('app.admin.allusers')->with($data);
    }
    function election_applications(){
    	return view('app.admin.election_applications');
    }
}
