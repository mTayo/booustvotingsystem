<?php

namespace App\Http\Controllers\app;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
class profileController extends Controller{

    function view_profile(Request $request){
    	$id=session('id');

    	if ($request->isMethod('post')) {
    		 $this->validate($request,[
                'change_name' =>'required',
                'change_email'=> 'required',
                'change_password'=>'sometimes|required|min:8',
                'confirm_password'=>'sometimes|required|same:change_password',
                'occupation'=>'required',
            ]);

    		$profile             = User::find($id);
    		$profile->full_name  = $request->get('change_name');
    		$profile->email      = $request->get('change_email');
    		$profile->occupation = $request->get('occupation');
    		$profile->password   = bcrypt($request->get('change_password'));
    		$profile->save();
    		return redirect('/viewprofile')->with('success', 'profile updated');

    	}
		$profile=User::where('id',$id)->first();
		$data['user']=$profile;
		return view('app.profile.view')->with($data);
    	
    }
    function suspend($id){
        $suspend=User::find($id);
      if($suspend->role==4){
           return redirect('/allusers')->with('error', 'user already suspended'); 
        }
        $suspend->role=4;
        $suspend->save();
        return redirect('/allusers')->with('success', 'user suspended');

    }
    function activate($id){
          $activate=User::find($id);
        if($activate->role!=4){
           return redirect('/allusers')->with('error', 'user already active'); 
        }
        $activate->role=3;
        $activate->save();
        return redirect('/allusers')->with('success', 'user now active');
    }
    function toadmin($id){
        $admin=User::find($id);
        if($admin->role==1){
            return redirect('/allusers')->with('error', 'Already Admin!'); 
        }elseif ($admin->role==4) {
            return redirect('/allusers')->with('error', 'activate account first!');
        }
        $admin->role=1;
        $admin->save();
        return redirect('/allusers')->with('success', 'user now Admin after next login!');
    }
  
}
