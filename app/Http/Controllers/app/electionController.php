<?php

namespace App\Http\Controllers\app;

use Illuminate\Http\Request;
use App\Models\Election;
use App\Models\ElectionCategory;
use App\Models\ElectionApplication;
use App\Models\ElectionVote;
use App\Models\VotersRecord;
use App\Http\Controllers\Controller;
use DB;

class electionController extends Controller
{   

    function index(){
        $elections = Election::all();
        $data = [];
        $data['elections'] = $elections;
        return view('app.elections.index')->with($data);
    }

    function setupElection(Request $request, $id = null){

        $election = $id == null? new Election : Election::find($id);
        if($request->isMethod('post')){

            $this->validate($request,[
                'election_title' =>'required',
                'election_description' => 'required',
                'start_date' => 'required|date',
                'end_date' =>'required|date|after_or_equal:start_date'
            ]);

            if($id == null){
                $election->status = 0;
                $election->created_by = session('id'); 
            }
            $election->title = $request->election_title;
            $election->description = $request->election_description;
            $election->start_date  = date('Y-m-d h:i:s', strtotime($request->start_date));
            $election->end_date    = date('Y-m-d h:i:s', strtotime($request->end_date));
            $election->save();
            return redirect('election/view/'.$election->id)->with('success','Election saved successfully');
        }
        return view('app.elections.setup_election')->with('election',$election);
    }

    function setupCategory(Request $request, $id = null){
        
        if($request->isMethod('post')){
            $this->validate($request,[
                'position' =>'required',
                'status' => 'required',
            ]);
            $position = $id == null? new ElectionCategory : ElectionCategory::find($id);
            $position->election_id = $request->election_id;
            $position->category = $request->position;
            $position->status = $request->status;
            $position->created_by = session('id');
            $position->save();
            if ($id == null) {
                return redirect()->back()->with('success','Position added successfully');
            }else{
                return redirect()->back()->with('success','Position updated successfully');
            }
            
        }
    }

    function viewElection($id){
        $election = Election::where('id',$id)->first();
        $data = [];
        $data['election'] = $election;
        if($election->status < 2){
            $categories = ElectionCategory::where('election_id',$election->id)->get();
            $data['categories'] = $categories;
        }
        if($election->status == 2){
            $result = self::getElectionResult($id);
            $data['results'] = $result; 
        }
        return view('app.elections.view_election')->with($data);
    }

    function getElectionResult($id){
        $positions   = ElectionCategory::where([['election_id',$id],['status',1]])->get();
        $votes       = ElectionVote::select('election_votes.*','election_votes.position as election_category','users.full_name','users.vin')->join('users','election_votes.applicant_id','=','users.id')
                                         ->where([['election_id',$id]])->get();
        foreach ($positions as $position) {
            $position->contestants = self::sortCategoryContestants($position->id,$votes);
        }
        return $positions;

    }

    function viewApplications($id){
        $election = Election::find($id);
        if($election == null){
            abort(404);
        }
        $categories = ElectionCategory::where([['election_id',$id],['status',1]])->get();
        $applications = ElectionApplication::select('election_applications.*','election_categories.category','users.full_name','users.vin')->join('election_categories','election_applications.election_category','=','election_categories.id')->join('users','election_applications.applicant_id','=','users.id')
                                        ->where('election_applications.election_id',$id)
                                        ->where(function($query){
                                            if(session('role') == 3){
                                                $query->where('election_applications.applicant_id',session('id'));
                                            }
                                        })->get();
        $data = [];
        $data['election'] = $election;
        $data['election_categories'] = $categories;
        $data['applications'] = $applications;
        return view('app.elections.applications')->with($data);
    }

    function sendApplication(Request $request){
       
        $this->validate($request,[
            'position' => 'required',
            'reason'   => 'required'
        ]);
        //check existing application if user has applied for that position already
        $existing_application = ElectionApplication::where([['election_id',$request->election_id],['election_category',$request->position],['applicant_id',session('id')]])->count();
        if ($existing_application > 0) {
            return redirect()->back()->with('error','You have applied for this position already');
        }
        $application = new ElectionApplication;
        $application->election_id = $request->election_id;
        $application->applicant_id = session('id');
        $application->election_category = $request->position;
        $application->status = 0;
        $application->reason = $request->reason;
        $application->save();
        return redirect()->back()->with('success','Application successfully sent');

    }

    function startElection($id){
        $start_election = Election::where('id',$id)->update(['status'=>1]);
        if ($start_election) {
            return redirect()->back()->with('success','Election started successfully');
        }else{
            return redirect()->back()->with('error','An error occured');
        }
    }

    function concludeElection($id){
        $conclude_election = Election::where('id',$id)->update(['status'=>2]);
        if ($conclude_election) {
            return redirect()->back()->with('success','Election concluded successfully');
        }else{
            return redirect()->back()->with('error','An error occured');
        }
    }

    function cancelElection($id){

        $cancel_election = Election::where('id',$id)->update(['status'=>3]);
        if ($cancel_election) {
            return redirect()->back()->with('success','Election cancelled successfully');
        }else{
            return redirect()->back()->with('error','An error occured');
        }
    }

    function approveApplication($id){
        $application = ElectionApplication::where('id',$id)->first();

        $application->status = 1;
        //store applicant record in election votes table
        $election_vote = new ElectionVote;
        $election_vote->applicant_id = $application->applicant_id;
        $election_vote->election_id  = $application->election_id;
        $election_vote->position     = $application->election_category;
        $election_vote->status       = 1;
        $election_vote->vote_count   = 0;

        DB::beginTransaction();
            $application->save();
            $election_vote->save();
        DB::commit();
        
        return redirect()->back()->with('success','Application approval successful');

    }

    function declineApplication($id){
        $decline_application = ElectionApplication::where('id',$id)->update(['status'=>2]);
        if($decline_application){
            return redirect()->back()->with('success','Application successfully declined');
        }else{
            return redirect()->back()->with('error','An error occured');
        }
    }

    function deleteElectionCategory($id){
        $delete_category = ElectionCategory::where('id',$id)->delete();
        if($delete_category){
            return redirect()->back()->with('success','Position deleted successfully');
        }else{
            return redirect()->back()->with('error','An error occured');
        } 
    }

    function polls($id){

        $election    = Election::find($id);
        $voters_record = VotersRecord::where([['voter_id',session('id')],['election_id',$id]])->first();
        
        $positions   = ElectionCategory::where([['election_id',$id],['status',1]])
                                        ->where(function($query)use($voters_record){
                                            if ($voters_record != null) {
                                                $query->whereNotIn('id',explode(',',$voters_record->positions_voted));
                                            }
                                        })->get();
     
        $contestants = ElectionApplication::select('election_applications.*','users.full_name','users.vin')
                                          ->join('users','election_applications.applicant_id','=','users.id')
                                          ->where([['election_id',$id],['status',1]])
                                          ->where(function($query)use($voters_record){
                                            if ($voters_record != null) {
                                                $query->whereNotIn('election_category',explode(',',$voters_record->positions_voted));
                                            }
                                          })->get();
        foreach ($positions as $position) {
            $position->contestants = self::sortCategoryContestants($position->id,$contestants);
        }
        $data = [];

        $data['election'] = $election;
        $data['positions'] = $positions;
        $data['voters_record'] = $voters_record;
        return view('app.elections.polls')->with($data);

    }

    function sortCategoryContestants($cat_id, $contestants){
        $result = [];
        foreach ($contestants as $contestant) {
            if ($contestant->election_category == $cat_id) {
                $result[] = $contestant;
            }
        }
        return $result;
    }

    function castVote($election_id, $position_id, $contestant_id){

        $voters_record = VotersRecord::where([['voter_id',session('id')],['election_id',$election_id]])->first();
        $candidate = ElectionVote::where([['election_id',$election_id],['applicant_id',$contestant_id],['position',$position_id]])->first();
        //voter's logic
        if ($voters_record == null) {
            $voters_record = new VotersRecord;
            $voters_record->election_id = $election_id;
            $voters_record->voter_id   = session('id');
            $voters_record->positions_voted = $position_id;
        }else{
            $positions_voted = explode(',',$voters_record->positions_voted);
            $positions_voted[] = $position_id;
            $voters_record->positions_voted = implode(',', $positions_voted);
        }
        //candidate's logic
        $vote_count = $candidate->vote_count + 1;
        $candidate->vote_count = $vote_count;
        if ($candidate->voters == null) {
            $candidate->voters = session('id');
        }else{
            $voters = explode(',', $candidate->voters);
            $voters[] = session('id');
            $candidate->voters = implode(',', $voters);
        }
        DB::beginTransaction();
            $voters_record->save();
            $candidate->save();
        DB::commit();
        return redirect()->back()->with('success','Vote has been processed successfully');
    }

    function electionresults(){
    	return view('app.elections.electionresults');
    }

    function vote(){
    	return view('app.elections.vote');
    }
}

