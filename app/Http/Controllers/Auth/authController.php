<?php

namespace App\Http\Controllers\auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Auth;
    class authController extends Controller{
        function login(){
        	return view('auth.login');
        }

        function logout(){
            Auth::logout();
            return redirect('login');
        }

        function verify_login(Request $request){
            $this->validate($request,[
                    'email'=>'required',
                    'password'=>'required',       
                ]);
            if(Auth::attempt(['email'=>$request->email,'password'=>$request->password])){
            $user = Auth::user();
            if($user->role==4){
                return redirect()->back()->with('error', 'you have been suspended');
            }else{
            $session_details = [
                'id'   => $user->id,
                'name' => $user->full_name,
                'vin'  => $user->vin,
                'role' => $user->role,
                'email'=> $user->email,
                'occupation'=>$user->occupation
            ];

            session($session_details);
            return redirect()->intended('/viewprofile');
            }}else{
                return redirect()->back()->with('error','username or password incorrect');
                }
        }
        function register(){
        	return view('auth.registration');
        }
        function verify_registration(Request $request){
        	$this->validate($request,[
                    'fullname' => 'required',
                    'email'=>'required|unique:users',
                    'occupation' => 'required',
                    'password'=>'required|min:8',
                    'confirm_password'=>'required|same:password',
                ]);
            $user=new User();
            $user->full_name = $request->fullname;
            $user->email = $request->email;
            $user->occupation = $request->occupation;
            $user->password = bcrypt($request->password);
            $user->role = 1;
            $user->vin =  uniqid();
            $user->save();
            return redirect('login')->with('success','registration successful');
        }
    }
