@extends('app.layouts.master')

@section('content')
                    <div class="card">
                        {{--display flash data--}}
                            @if(session('success'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    {{session('success')}}
                                    <button type="button" class="close" data-dismiss="alert">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                            @if(session('error'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    {{session('error')}}
                                    <button type="button" class="close" data-dismiss="alert">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            @endif
                        <!--Card content-->
                        <div class="card-body">
                        	<h3 class="blue-text mb-2 mb-sm-0 pt-1 "style="font-weight: 300;">All Users</h3>
                            <!-- Table  -->
                            <table class="table table-hover">
                                <!-- Table head -->
                                <thead class="blue-grey lighten-4">
                                    <tr>
                                        <th>#</th>
                                        <th>Full name</th>
                                        <th>Email</th>
                                        <th>VIN</th>
                                        <th>Occupation</th>
                                        <th>Action</th>
                                        <th>Admin</th>
                                    </tr>
                                </thead>
                                <!-- Table head -->
                                @if(count($user)>0)
                                <!-- Table body -->
                                <tbody>
                                    @foreach($user as $key => $users)
                                    <tr>
                                        <th scope="row">{{$key++}}</th>
                                        <td>{{$users->full_name}}</td>
                                        <td>{{$users->email}}</td>
                                        <td>{{$users->vin}}</td>
                                        <td>{{$users->occupation}}</td>
                                         <td class="text-center">
                                        <a href="{{url('profile/suspend/'.$users->id)}}" class="blue-grey-text"><i class="fa fa-trash red-text"></i></a>
                                         <a href="{{url('profile/activate/'.$users->id)}}" class="blue-grey-text"><i class="fa-hand-peace-o green-text"></i></a>
                                        </td>
                                        <td class="text-center">
                                            <a href="{{url('profile/admin/'.$users->id)}}" class="blue-grey-text"><i class="fa fa-check"></i></a>
                                        </td>

                                       
                                        
                                    </tr>
                                   @endforeach
                                </tbody>
                                <!-- Table body -->
                                @endif
                            </table>
                            <!-- Table  -->

                        </div>
                    </div>    
@endsection