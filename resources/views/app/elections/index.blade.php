@extends('app.layouts.master')
	@section('content')
	<div class="row">
		<div class="col-md-12 mb-12">
			<div class="card">
                <h3 class="my-1 mb-sm-0 p-2 d-flex justify-content-between align-items-center border-bottom">
                    Election List
                    <a class="btn btn-outline-info btn-sm" href="{{url('election/setup')}}">Create Election</a>
                </h3>
                <!--Card content-->
                <div class="card-body p-2">
                    <!-- Table  -->
                    @if(count($elections) > 0)
                        <div class="table-responsive">
                            <table class="table table-hover">
                                <!-- Table head -->
                                <thead class="mdb-color white-text">
                                    <tr>
                                        <th>SN</th>
                                        <th>Election title</th>
                                        <th>Start date</th>
                                        <th>End date</th>
                                        <th class="text-center">Status</th>
                                        <th class="text-center">Action</th>
                                    </tr>
                                </thead>
                                <!-- Table head -->

                                <!-- Table body -->
                                <tbody>
                                    @foreach($elections as $key => $election)
                                    <tr>
                                        <th scope="row">{{$key + 1}}</th>
                                        <td>{{$election->title}}</td>
                                        <td>{{date('Y-m-d',strtotime($election->start_date))}}</td>
                                        <td>{{date('Y-m-d',strtotime($election->end_date))}}</td>
                                        <td class="text-center">
                                            <?php
                                                switch ($election->status) {
                                                    case 0:
                                                        $badge = 'badge-warning';
                                                        $status = 'Pending';
                                                        break;
                                                    case 1:
                                                        $badge = 'badge-info';
                                                        $status = 'Active';
                                                        break;
                                                    case 2:
                                                        $badge = 'badge-success';
                                                        $status = 'Completed';
                                                        break;
                                                    case 3:
                                                        $badge = 'badge-danger';
                                                        $status = 'Cancelled';
                                                        break;
                                                }
                                            ?>
                                            <span class="badge {{$badge}}">{{$status}}</span>
                                        </td>
                                        <td class="text-center">
                                            <a href="{{url('election/view/'.$election->id)}}" class="blue-grey-text"><i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <!-- Table body -->
                            </table>
                        </div>
                    @else
                        <p class="text-center my-1">No Records Available</p>
                    @endif
                    <!-- Table  -->
                </div>
            </div>  
        </div>  
    </div>
@endsection