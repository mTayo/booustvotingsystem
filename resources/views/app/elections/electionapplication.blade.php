@extends('app.layouts.master')
@section('content')
    <div class="row">
         <div class="col-md-8 mb-8" style="margin:0 auto;">
        	<div class="card">

                <h5 class="card-header info-color white-text text-center py-4 blue-grey lighten-4">
                    <strong>Apply for Election</strong>
                </h5>

                <!--Card content-->
                <div class="card-body px-lg-5 pt-0">

                    <!-- Form -->
                    <form class="text-center"  method="post" action="#">

                        <!-- Name -->
                        <div class="md-form mt-3">
                            <input type="text" name="election_title" class="form-control">
                            <label for="Electiontitle">Election Title</label>
                        </div>

                        <!-- E-mail -->
                        <div class="md-form">
                            <input type="text" name="position_contested" class="form-control">
                            <label for="Position">Position Contested for</label>
                        </div>

                          <div class="md-form">
                            <input type="email" name="email"  class="form-control">
                            <label for="email">E-mail</label>
                        </div>
                        <div class="md-form">
                            <input type="text" name="occupation" class="form-control">
                            <label for="occupation">Occupation</label>
                        </div>
                        <!-- Subject -->
                       

                      
                        <div class="md-form">
                           <input type="hidden"  name="vin" value="56">
                        </div>

                      

                        <!-- Send button -->
                       
                        <button class="btn btn-info my-4 btn-block" type="submit">Submit</button>


                    </form>
                    <!-- Form -->

                </div>

            </div>
        </div>
    </div>

@endsection