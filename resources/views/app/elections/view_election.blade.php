@extends('app.layouts.master')
@section('content')

<div class="modal fade" id="electionCategoryForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Election Position</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-2">
                <form method="post" action="{{url('election/category/setup')}}" id="position-form">
                    @csrf
                    <input type="hidden" name="election_id" value="{{$election->id}}">
                     <div class="md-form mb-4">
                        <input type="text" id="elect-pos" class="form-control " name="position" placeholder=" " value="{{old('position')}}">
                        <label for="elect-pos">Position</label>
                         @if($errors->has('position'))
                        <span class="form-text text-danger">{{$errors->first('position')}}</span>
                        @endif
                    </div>
                    <div class="form-row">
                        <div class="col-12 pt-4">
                            <select class="mdb-select" name="status" id="pos-status">
                                <option value="1">Enabled</option>
                                <option value="2">Disabled</option>
                            </select>
                            <label>Status</label>
                             @if($errors->has('status'))
                            <span class="form-text text-danger">{{$errors->first('status')}}</span>
                            @endif
                        </div>
                    </div>
                    
                    <div class="text-center">
                        <button class="btn btn-default btn-sm">save</button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>
@if($errors->any())
    <script type="text/javascript">
        $('#electionCategoryForm').modal('show');
    </script>
@endif
<div class="row">
     <div class="col-md-8 mx-auto">
        <div class="text-center mb-2">
            <a href="{{url('election/view/'.$election->id)}}" class="btn btn-sm {{Request::is('election/view/*')? 'btn-default' : 'btn-outline-default'}} ">Summary</a>
            <a href="{{url('election/applications/'.$election->id)}}" class="btn btn-sm {{Request::is('election/applications/*')? 'btn-default' : 'btn-outline-default'}} ">Applications</a>
            <a href="{{url('election/polls/'.$election->id)}}" class="btn btn-sm {{Request::is('election/polls/*')? 'btn-default' : 'btn-outline-default'}} ">Polls</a>
        </div>
        {{--display flash data--}}
        @if(session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if(session('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{session('error')}}
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        {{--election details--}}
    	<div class="card rounded-0 mb-3">
            <h5 class="p-2 rounded-0 white d-flex justify-content-between border-bottom align-items-center">
                <strong>Election Details &nbsp;<a href="{{url('election/setup/'.$election->id)}}"><i class="fa fa-pencil"></i></a></strong>
                @if(session('role')==1 && $election->status < 1)
                    <button class="btn btn-outline-info btn-sm m-0" data-toggle="modal" data-target="#electionCategoryForm">Add Position</button>
                @endif
            </h5>
            {{--election details--}}
            <div class="card-body p-2">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th class="w-25">Title :</th>
                            <td>{{$election->title}}</td>
                        </tr>
                        <tr>
                            <th>Description :</th>
                            <td>{{$election->description}}</td>
                        </tr>
                        <tr>
                            <th>Start Date :</th>
                            <td>{{date('Y-m-d', strtotime($election->start_date))}}</td>
                        </tr>
                        <tr>
                            <th>End Date :</th>
                            <td>{{date('Y-m-d', strtotime($election->end_date))}}</td>
                        </tr>
                        <tr>
                            <th>Status :</th>
                            <td>
                                <?php
                                    $status = '';
                                    switch ($election->status) {
                                        case 0:
                                            $status = 'Upcoming';
                                            break;
                                        case 1:
                                            $status = 'Active';
                                            break;
                                        case 2:
                                            $status = 'Concluded';
                                            break;
                                        case 3:
                                            $status = 'Cancelled';
                                            break;
                                    }
                                ?>
                                {{$status}}
                            </td>
                        </tr>
                    </tbody>
                </table>
                @if(session('role') < 3)
                   @if($election->status == 0)
                    <a href="{{url('election/start/'.$election->id)}}" class="btn btn-outline-info btn-sm">
                        Start Election
                    </a>
                    <a href="{{url('election/cancel/'.$election->id)}}" onclick="return confirm('Do you want to cancel this election? All ongoing processes would be halted')" class="btn btn-outline-danger btn-sm">
                        Cancel Election
                    </a>
                    @endif
                    @if($election->status == 1)
                    <a href="{{url('election/conclude/'.$election->id)}}" onclick="return confirm('Do you want to conclude this election? results would be collated once you proceed')" class="btn btn-outline-success btn-sm">
                        Conclude Election
                    </a>
                    @endif
                @endif
            </div>
        </div>
        {{--election categories--}}
        @if($election->status < 2)
            <div class="card rounded-0 mb-3">
                <div class="card-body p-0">
                    <h5 class="p-2 mb-0 border-bottom">Positions Available</h5>
                    @if(count($categories) > 0)
                        <table class="table">
                            <thead class="mdb-color white-text">
                                <tr>
                                    <th>Position</th>
                                    <th class="text-center">Status</th>
                                    @if($election->status == 0)
                                    <th class="text-center">Action</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $category)
                                    <tr>
                                        <td>{{$category->category}}</td>
                                        <td class="text-center">
                                            {!!$category->status == 1? '<i class="fa fa-check green-text"></i>' : '<i class="fa fa-times red-text"></i>' !!}
                                        </td>
                                        @if($election->status == 0)
                                        <td class="text-center">
                                            <a href="javascript:void(0)" class="update-position" data-url="{{url('election/category/setup/'.$category->id)}}" data-position="{{$category->category}}" data-status="{{$category->status}}">
                                                <i class="fa fa-pencil blue-grey-text"></i>
                                            </a>
                                            <a href="{{url('election/category/delete/'.$category->id)}}"><i class="fa fa-trash red-text"></i></a>
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    @else
                        @if(session('role') < 3)
                        <p class="small my-2 text-center">No Position Available. Click <a href="javascript:void(0)" data-toggle="modal" data-target="#electionCategoryForm">Here</a> to create a position</p>
                        @endif
                    @endif
                </div>
            </div>
        @endif
        {{--election result--}}
        @if($election->status == 2)
            <div class="card">
                <div class="card-body p-2">
                    <h5 class="p-2 mb-2 border-bottom">Result</h5>
                    <div id="accordion">
                    @foreach($results as $key => $result)
                        <div class="border-bottom mb-2">
                            <h6 class="text-center font-weight-bold" data-toggle="collapse" href="#category-{{$result->id}}">
                                <a href="javascript:void(0)">{{$result->category}}</a>
                            </h6>
                            <div class="collapse {{$key == 0? 'show' :''}}" id="category-{{$result->id}}" data-parent="#accordion">
                                <span class="d-block small mb-2">
                                    {{count($result->contestants) > 1 ? 'Contestants' : 'Contestant'}}
                                </span>
                                <table class="table">
                                    <thead class="mdb-color white-text">
                                        <tr>
                                            <th>
                                                Name
                                            </th>
                                            <th>
                                                VIN
                                            </th>
                                            <th>
                                                Vote Count
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($result->contestants as $contestant)
                                        <tr>
                                            <td>{{$contestant->full_name}}</td>
                                            <td>{{$contestant->vin}}</td>
                                            <td>{{$contestant->vote_count}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    @endforeach
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>

@endsection