@extends('app.layouts.master')
@section('content')

@if($election->status == 0)
    <div class="modal fade" id="applicationForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header text-center">
                    <h4 class="modal-title w-100 font-weight-bold">Application Form</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body p-2">
                    @if(count($election_categories) == 0)
                        <p class="red-text border-left-danger text-center">
                            No position available for application at the moment.
                        </p>
                    @else
                        <form method="post" action="{{url('election/apply')}}">
                            @csrf
                            <input type="hidden" name="election_id" value="{{$election->id}}">
                            <div class="form-row">
                                <div class="col-12 pt-4">
                                    <select class="mdb-select" id="elect-cat" name="position" required>
                                        @foreach($election_categories as $category)
                                            <option value="{{$category->id}}">{{$category->category}}</option>
                                        @endforeach
                                    </select>
                                    <label for="elect-cat">Position</label>
                                    @if($errors->has('position'))
                                        <small class="red-text">{{$errors->first('position')}}</small>
                                    @endif
                                </div>
                            </div>
                            <div class="md-form mt-2">
                                <label for="app-reason">Reason For Application</label>
                                <textarea class="md-textarea form-control" name="reason" rows="2" id="app-reason"></textarea>
                                  @if($errors->has('reason'))
                                    <small class="red-text">{{$errors->first('reason')}}</small>
                                @endif
                            </div>
                            
                            <div class="text-right">
                                <button class="btn btn-outline-default btn-sm">
                                    <i class="fa fa-send"></i> &nbsp; Send Application
                                </button>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @if($errors->any())
        <script type="text/javascript">
            $('#applicationForm').modal('show');
        </script>
    @endif
@endif
<div class="row">
     <div class="col-md-8 mx-auto">
        <div class="text-center mb-2">
            <a href="{{url('election/view/'.$election->id)}}" class="btn btn-sm {{Request::is('election/view/*')? 'btn-default' : 'btn-outline-default'}} ">Summary</a>
            <a href="{{url('election/applications/'.$election->id)}}" class="btn btn-sm {{Request::is('election/applications/*')? 'btn-default' : 'btn-outline-default'}} ">Applications</a>
             <a href="{{url('election/polls/'.$election->id)}}" class="btn btn-sm {{Request::is('election/polls/*')? 'btn-default' : 'btn-outline-default'}} ">Polls</a>
        </div>
        {{--display flash data--}}
        @if(session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if(session('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{session('error')}}
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        {{--application list--}}
        <div class="card rounded-0 mb-3">
            <h5 class="p-2 rounded-0 white d-flex justify-content-between border-bottom align-items-center">
                <strong>{{session('role') == 3? 'My Applications' : 'Application List' }}</strong>
                @if($election->status == 0)
                <button class="btn btn-outline-info btn-sm m-0" data-toggle="modal" data-target="#applicationForm">Apply</button>
                @endif
            </h5>
            <div class="card-body p-2">
                @foreach($applications as $application)
                    <div class="border-bottom py-1">
                        <div class="d-flex justify-content-between mb-2">
                            <span class="blue-grey-text font-weight-bold">{{ucwords($application->full_name)}}</span>
                            <?php
                                switch ($application->status) {
                                    case 0:
                                        $status = 'Pending Approval';
                                        $icon  = 'fa-circle';
                                        $text_color = 'grey-text';
                                        break;
                                    case 1:
                                        $status = 'Approved';
                                        $icon = 'fa-check';
                                        $text_color = 'green-text';
                                        break;
                                    case 2:
                                        $status = 'Declined';
                                        $icon = 'fa-times';
                                        $text_color = 'red-text';
                                        break;
                                }
                            ?>
                            <small class="{{$text_color}}"><i class="fa {{$icon}}"></i> {{$status}}</small>
                        </div>
                        <div class="row small">
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-sm-2 font-weight-bold">V I N</div>
                                    <div class="col-sm-9">#{{$application->vin}}</div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-sm-2 font-weight-bold">Position</div>
                                    <div class="col-sm-9">{{$application->category}}</div>
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row">
                                    <div class="col-sm-2 font-weight-bold">Reason </div>
                                    <div class="col-sm-9">{{$application->reason}}</div>
                                </div>
                            </div>
                        </div>
                        @if($application->status == 0 && session('role') < 3 && $election->status == 0)
                            <div class="small my-2 d-flex justify-content-between">
                                <span>
                                    <a href="{{url('election/applications/approve/'.$application->id)}}" class="green-text"><i class="fa fa-check"></i> Approve</a> &nbsp;
                                    <a href="{{url('election/applications/decline/'.$application->id)}}" class="red-text"><i class="fa fa-times"></i> Decline</a>
                                </span>
                                <span>Sent : {{date('Y-m-d', strtotime($application->created_at))}}</span>
                            </div>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>

    </div>
</div>

@endsection