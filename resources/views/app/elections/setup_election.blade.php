@extends('app.layouts.master')
@section('content')
<?php
    $post_url = $election->id == null? url('election/setup') : url('election/setup/'.$election->id);
    $edit_mode = $election->id != null?:false;
?>
	 <div class="row">
         <div class="col-md-8 mb-8" style="margin:0 auto;">
        	<div class="card">

                <h5 class="card-header info-color white-text text-center py-4 blue-grey lighten-4">
                    <strong>Create Election</strong>
                </h5>

                <!--Card content-->
                <div class="card-body px-lg-5 pt-0">

                    <!-- Form -->
                    <form method="post" action="{{$post_url}}">
                        @csrf
                        <!-- Name -->
                        <div class="md-form mt-3">
                            <input type="text" name="election_title" class="form-control" value="{{$edit_mode? $election->title : old('election_title')}}">
                            <label for="Electiontitle">Election Title</label>
                        </div>
                        
                        <!-- E-mail -->
                        <div class="md-form">
                            <input type="text" name="election_description" class="form-control" value="{{$edit_mode? $election->description : old('election_description')}}">
                            <label for="Position">Election description</label>
                        </div>

                          <div class="md-form">
                             <input type="text" id="start_date" class="form-control  date-picker" name="start_date" data-value="{{$edit_mode? date('Y-m-d',strtotime($election->start_date)) : ''}}">
                            <label for="email">Start date</label>
                        </div>
                        <div class="md-form">
                        	 <input type="text" id="end_date" class="form-control  date-picker" name="end_date" data-value="{{$edit_mode? date('Y-m-d',strtotime($election->end_date)) : ''}}">
                          
                            <label for="end_date">End date</label>
                        </div>
                        <!-- Send button -->
                       
                        <button class="btn btn-info my-4 btn-block" type="submit">Submit</button>


                    </form>
                    <!-- Form -->

                </div>

            </div>
        </div>
    </div>
@endsection