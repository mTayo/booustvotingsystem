@extends('app.layouts.master')
@section('content')

<div class="row">
     <div class="col-md-8 mx-auto">
        <div class="text-center mb-2">
            <a href="{{url('election/view/'.$election->id)}}" class="btn btn-sm {{Request::is('election/view/*')? 'btn-default' : 'btn-outline-default'}} ">Summary</a>
            <a href="{{url('election/applications/'.$election->id)}}" class="btn btn-sm {{Request::is('election/applications/*')? 'btn-default' : 'btn-outline-default'}} ">Applications</a>
            <a href="{{url('election/polls/'.$election->id)}}" class="btn btn-sm {{Request::is('election/polls/*')? 'btn-default' : 'btn-outline-default'}} ">Polls</a>
        </div>
        {{--display flash data--}}
        @if(session('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if(session('error'))
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                {{session('error')}}
                <button type="button" class="close" data-dismiss="alert">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        {{--election details--}}
    	<div class="card rounded-0 mb-3">
            <div class="card-body p-2">
                <div class="d-flex justify-content-between border-bottom pb-1">
                    <b>{{$election->title}}</b>
                    <?php
                        $status = '';
                        switch ($election->status) {
                            case 0:
                                $status = 'Upcoming';
                                $text_color = 'amber-text';
                                break;
                            case 1:
                                $status = 'Active';
                                $text_color = 'blue-text';
                                break;
                            case 2:
                                $status = 'Concluded';
                                $text_color = 'green-text';
                                break;
                            case 3:
                                $status = 'Cancelled';
                                $text_color = 'red-text';
                                break;
                        }
                    ?>
                    <small class="{{$text_color}}">
                       <i class="fa fa-circle"></i> {{$status}}
                    </small>
                </div>
                <div class="py-2">
                    {{$election->description}}
                </div>
                <div class="small my-1 font-weight-bold text-muted">
                    {{date('jS M, Y', strtotime($election->start_date))}} &nbsp; - &nbsp; {{date('jS M, Y', strtotime($election->end_date))}}
                </div>
            </div>
        </div>
        @if($election->status == 1)
            {{--positions--}}
            <div class="card rounded-0">
                <div class="card-body p-2">
                    <div class="pb-2 mb-2 border-bottom">
                        <b>Electoral Positions</b>
                    </div>
                    @if(count($positions) == 0)
                        <div class="text-center">
                            @if($voters_record == null)
                                <i class="fa fa-warning"></i> No Positions available.
                            @else
                                <i class="fa fa-info-circle"></i> You have no pending electoral position to vote for.
                            @endif
                        </div>
                    @else
                        <div id="accordion">
                        @foreach($positions as $key => $position)
                            <div class="border-bottom mb-2">
                                <h6 class="text-center font-weight-bold" data-toggle="collapse" href="#category-{{$position->id}}">
                                    <a href="javascript:void(0)">{{$position->category}}</a>
                                </h6>
                                <div class="collapse {{$key == 0? 'show' :''}}" id="category-{{$position->id}}" data-parent="#accordion">
                                    <span class="d-block small mb-2">
                                        {{count($position->contestants) > 1 ? 'Contestants' : 'Contestant'}}
                                    </span>
                                    @foreach($position->contestants as $contestant)
                                        <dl class="row small mb-0">
                                            <dt class="col-md-3">Name</dt> 
                                            <dd class="col-md-9"> {{$contestant->full_name}}</dd>
                                            <dt class="col-md-3">VIN</dt>
                                            <dd class="col-md-9">{{$contestant->vin}}</dd>
                                            <dt class="col-md-3">Manifest</dt> 
                                            <dd class="col-md-9 mb-0">{{$contestant->reason}}</dd>
                                        </dl>
                                        <a href="{{url('election/polls/vote/'.$election->id.'/position/'.$position->id.'/contestant/'.$contestant->applicant_id)}}" class="btn btn-flat btn-outline-success btn-sm mx-0"><i class="fa fa-check"></i> Vote</a>
                                    @endforeach
                                </div>
                            </div>
                        @endforeach
                        </div>
                    @endif
                </div>
            </div>
        @endif
    </div>
</div>

@endsection