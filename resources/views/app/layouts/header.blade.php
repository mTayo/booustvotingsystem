	<!--Main Navigation-->
	<header>
		<!-- SideNav slide-out button -->
		  <!-- <a  data-activates="slide-out" class="btn btn-primary p-3 button-collapse"><i class="fa fa-bars"></i></a> -->
		  <!--Navbar-->
			<nav class="navbar navbar-expand-lg navbar-dark double-nav">

			   <!-- SideNav slide-out button -->
				  <div class="float-left">
				      <a  data-activates="slide-out" class="button-collapse">
				      		<i class="fa fa-bars"></i>
				      </a>		
				      	<div class="breadcrumb-dn mr-auto">
    						<p>Booust Voting system  {{session('vin')}}'s space </p>
						</div>
						</div>
						<!-- Links -->
						<ul class="nav navbar-nav nav-flex-icons ml-auto">
						    <li class="nav-item">
						        <a class="nav-link" href="{!! url('/')!!}">
						            <i class="fa fa-envelope"></i>
						            <span class="clearfix d-none d-sm-inline-block">Home</span>
						        </a>
						    </li>
						   
						
						</ul>

				       
				      
				  
				 
			</nav>
			<!--/.Navbar-->
		  <!-- Sidebar navigation -->
		  <div id="slide-out" class="side-nav fixed ">
		      <ul class="custom-scrollbar">
		          <!-- Logo -->
		          <li>
		              <div class="logo-wrapper waves-light "style="height: 32vh;">
		                  <a ><img src="img/logo.png" class="img-fluid flex-center"></a>
		              </div>
		          </li>
		          <!--/. Logo -->
		         
		          <!-- Side navigation links -->
		          <li>
		              <ul class="collapsible collapsible-accordion xx">
		              	@if(session('role')==1)
		                  <li><a class="collapsible-header waves-effect arrow-r xx" >
		                          Admin<i class="fa fa-angle-down rotate-icon"></i></a>
		                          <div class="collapsible-body">
		                          <ul>
		                              <li><a href="{!! url('/allusers')!!}" class="waves-effect">All Users</a>
		                              </li>
		                          </ul>
		                      </div>
		                     
		                  </li>
		                  @endif
		                  <li><a href="{!! url('/viewprofile')!!}">Profile<i class="fa fa-angle-down rotate-icon"></i></a>
		                  </li>
		                  <li><a class="collapsible-header waves-effect arrow-r" >Elections<i class="fa fa-angle-down rotate-icon"></i></a>
		                  	@if(session('role')==1)
		                  		<div class="collapsible-body">
		                          <ul>
		                              <li><a href="{!! url('/election/setup')!!}" class="waves-effect">Create new election</a>
		                              </li>
		                          </ul>
		                      </div>
		                       @endif
	                  		  <div class="collapsible-body">
		                          <ul>
		                              <li><a href="{!! url('/elections')!!}" class="waves-effect">Election lists</a>
		                              </li>
		                          </ul>
		                      </div>
		              
		          		</li>
		          		<li><a href="{!! url('/logout')!!}">logout<i class="fa fa-angle-down rotate-icon"></i></a>
		                  </li>
		          	</ul>
		          <!--/.
		           Side navigation links -->
		      </ul>
		      <div class="sidenav-bg dark"></div>
		  </div>
  			<!--/. Sidebar navigation -->
	</header>
	<!--Main Navigation-->