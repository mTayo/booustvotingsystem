<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Dashboard</title>
	<link rel="stylesheet" type="text/css" href="{{asset('/icons/font-awesome-4.7.0/css/font-awesome.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('/mdb/css/bootstrap.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('/mdb/css/mdb.min.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('/mdb/css/style.css')}}">
	<script src="{{asset('/mdb/js/jquery-3.3.1.min.js')}}"></script>
	<script src="{{asset('/mdb/js/popper.min.js')}}"></script>
	<script src="{{asset('/mdb/js/bootstrap.min.js')}}"></script>
	<script src="https://use.fontawesome.com/2b49900ca0.js"></script>
</head>
<body  class="fixed-sn light-blue-skin">

	@include('app.layouts.header')

	<!--Main layout    light-blue-skin-->
	<main>
		@yield('content')
	</main>
	<!--Main layout-->

	<!--Footer-->
	@include('app.layouts.footer')
	<!--Footer-->
	<script src="{{asset('/mdb/js/mdb.min.js')}}"></script>
	<script src="{{asset('/mdb/js/init.js')}}"></script>
	<script>
 
	</script>
	@stack('scripts')
</body>
</html>