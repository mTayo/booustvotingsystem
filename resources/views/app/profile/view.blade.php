@extends('app.layouts.master')
@section('content')

<div class="modal fade" id="profileModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header text-center">
                <h4 class="modal-title w-100 font-weight-bold">Update profile</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body p-2">
                <form method="post" action="{{url('/editprofile')}}">
                    @csrf
                   
                    <div class="md-form mb-4">
                        <input type="text" id="name" class="form-control " name="change_name" value="{{$user->full_name}}" >
                        <label  for="change_name">Change name</label>
                         @if($errors->has('change_name'))
                        <span class="form-text text-danger">{{$errors->first('change_name')}}</span>
                        @endif
                    </div>

                    <div class="md-form mb-4">
                           <input type="email" id="change_email" class="form-control mb-4" name="change_email" value="{{$user->email}}">
                        <label  for="email">Change Email</label>
                         @if($errors->has('change_email'))
                        <span class="form-text text-danger">{{$errors->first('change_email')}}</span>
                        @endif
                    </div>

                    <div class="md-form mb-4">
                        <input type="password" id="change_password" name="change_password" class="form-control mb-4"   >
                        <label  for="change_password">Change password</label>
                         @if($errors->has('change_password'))
                        <span class="form-text text-danger">{{$errors->first('change_password')}}</span>
                        @endif
                    </div>

                   <div class="md-form mb-4">
                        <input type="password" id="confirm_password" name="confirm_password" class="form-control mb-4"   >
                        <label  for="confirm_password">confirm password</label>
                         @if($errors->has('confirm_password'))
                        <span class="form-text text-danger">{{$errors->first('confirm_password')}}</span>
                        @endif
                    </div>

                    <div class="md-form mb-4">
                        <input type="text" id="occupation" name="occupation" class="form-control mb-4" value="{{$user->occupation}}" >
                        <label data-error="wrong" data-success="right" for="end-date">occupation</label>
                         @if($errors->has('occupation'))
                        <span class="form-text text-danger">{{$errors->first('occupation')}}</span>
                        @endif
                    </div>
                 
                    
                    <div class="text-center">
                        <button class="btn btn-default btn-sm">Submit</button>
                    </div>
                    
                </form>
            </div>
        </div>
    </div>
</div>  
@if($errors->any())
    <script type="text/javascript">
        $('#profileModal').modal('show');
    </script>
@endif
     {{--display flash data--}}
                @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{session('success')}}
                        <button type="button" class="close" data-dismiss="alert">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                @if(session('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{session('error')}}
                        <button type="button" class="close" data-dismiss="alert">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
         <div class="row">
                 <div class="col-md-8 mx-auto">
                    <div class="card rounded-0 mb-3">
                        <h5 class="p-2 rounded-0 white d-flex justify-content-between border-bottom align-items-center">
                            <strong>profile details&nbsp;<i class="fa fa-pencil small"></i></strong>
                            <button class="btn btn-outline-info btn-sm m-0" data-toggle="modal" data-target="#profileModal">Update profile</button>
                        </h5>
                    
                        <div class="card-body p-2">
                            <table class="table table-borderless">
                                <tbody>
                                    <tr>
                                        <th class="w-25">Full name :</th>
                                        <td>{{$user->full_name}}</td>
                                    </tr>
                                    <tr>
                                        <th>Email:</th>
                                        <td>{{$user->email}}</td>
                                    </tr>
                                    <tr>
                                        <th>VIN :</th>
                                        <td>{{$user->vin}}</td>
                                    </tr>
                                    <tr>
                                        <th>Occupation :</th>
                                        <td>{{$user->occupation}}</td>
                                    </tr>
                                    <tr>
                                        <th>Last updated :</th>
                                        <td>
                                        {{$user->updated_at}}
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Role :</th>
                                        <td>
                                                <?php
                                                    $user="";
                                                    switch (session('role')) {
                                                        case '1':
                                                            $user="Admin";
                                                            break;
                                                        case '2':
                                                            $user="user";
                                                            break;
                                                        case '3':
                                                            $user="user";
                                                            break;
                                                    }
                                                ?>
                                        {{$user}}
                                        </td>
                                    </tr>
                                     
                                </tbody>
                            </table>
                        </div>
                    </div>
            

                </div>



    </div>
  
@endsection