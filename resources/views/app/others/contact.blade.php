<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Booust voting system</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
   
    <link href="{{asset('/ncss/bootstrap.min.css')}}" rel="stylesheet" >
    <!-- Material Design Bootstrap -->
    <link href="{{asset('/ncss/mdb.min.css')}}" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="{{asset('/ncss/style.css')}}" rel="stylesheet">
    <!-- Validation scripts -->

</head>

<body>
    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark teal mb-5 mt fixed-top">

        <!-- Navbar brand -->
        <a class="navbar-brand" href="index.html">
            <img class="img-fluid" height="30" width="50" src="img/logo.png">
        </a>

        <!-- Collapse button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar"
            aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Collapsible content -->
        <div class="collapse navbar-collapse" id="navbar">

            <!-- Links -->
             <ul class="navbar-nav mr-auto nav-spacing">
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/')}}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/contact')}}">Contact</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/about')}}">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Demo</a>
                </li>

            </ul>
            <!-- Links -->

            <div class="form-inline">
                <a href="{!! url('/login')!!}"><button class="btn btn-outline-white btn-md my-2 my-sm-0 ml-3" type="submit">Login</button></a>
                <a href="{!! url('/register')!!}"><button class="btn btn-outline-white btn-md my-2 my-sm-0 ml-3" type="submit">Register</button></a>
            </div>
        </div>
        <!-- Collapsible content -->

    </nav>
    <!--/.Navbar-->

    <!-- Section -->
    <section class="container-fluid pt-5 my-5">
        <!-- Section heading -->
        <h2 class="h1-responsive font-weight-bold text-center my-5">Contact us</h2>
        <!-- Section description -->
        <p class="text-center w-responsive mx-auto mb-5">For Inquries about our products and services, please contact
            us through our email address or place a call to our number below below or fill the form below and type in
            your issues or inquires below, we assure you our 24\7 online assistances will provide a quick response
            between 24 hours. Thank you and have a nice day.</p>

        <!-- Grid row -->
        <div class="row">

            <!-- Grid column -->
            <div class="col-md-9 mb-md-0 mb-5">
                <form class="needs-validation" action="" id="contact-form" method="POST" novalidate>
                    <!-- Grid row -->
                    <div class="row">
                        <!-- Grid column -->
                        <div class="col-md-6">
                            <div class="md-form">
                                <i class=" px-1 fa fa-user prefix grey-text"></i>
                                <input type="text" id="name" name="name" class="form-control" required>
                                <label for="name" class="">Your name</label>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please provide a valid name.
                                </div>
                            </div>
                            
                        </div>
                        <!-- Grid column -->

                        <!-- Grid column -->
                        <div class="col-md-6">
                            <div class="md-form">
                                <i class="px-1 fa fa-envelope prefix grey-text"></i>
                                <input type="text" id="email" name="email" class="form-control" required>
                                <label for="email" class="">Your email</label>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please provide a valid email.
                                </div>
                            </div>
                                
                        </div>
                        <!-- Grid column -->

                    </div>
                    <!-- Grid row -->

                    <!-- Grid row -->
                    <div class="row">

                        <!-- Grid column -->
                        <div class="col-md-12">
                            <div class="md-form">
                                <i class=" px-1 fa fa-tag prefix grey-text"></i>
                                <input type="text" id="subject" name="subject" class="form-control" required>
                                <label for="subject" class="">Subject</label>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please input a subject.
                                </div>
                            </div>
                        </div>
                        <!-- Grid column -->

                    </div>
                    <!-- Grid row -->

                    <!-- Grid row -->
                    <div class="row">

                        <!-- Grid column -->
                        <div class="col-md-12">
                            <div class="md-form">
                                <i class="px-1 fa fa-pencil prefix grey-text"></i>
                                <textarea type="text" id="message" name="message" class="form-control md-textarea" rows="3"
                                    required></textarea>
                                <label for="message">Your message</label>
                                <div class="valid-feedback">
                                    Looks good!
                                </div>
                                <div class="invalid-feedback">
                                    Please input a message.
                                </div>
                            </div>
                        </div>
                        <!-- Grid column -->

                    </div>
                    <!-- Grid row -->
                    <div class="text-center text-md-left">
                        <button class="btn btn-teal btn-md" type="submit">Submit form</button>
                    </div>
                </form>
                
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-3 text-center">
                <ul class="list-unstyled mb-0">
                    <li>
                        <i class="fa fa-map-marker fa-2x teal-text"></i>
                        <p>Lagos, IKJ 102101, Nigeria</p>
                    </li>
                    <li>
                        <i class="fa fa-phone fa-2x mt-4 teal-text"></i>
                        <p>+ 01 234 567 89</p>
                    </li>
                    <li>
                        <i class="fa fa-envelope fa-2x mt-4 teal-text"></i>
                        <p class="mb-0">contact@example.com</p>
                    </li>
                </ul>
            </div>
            <!-- Grid column -->
        </div>
        <!-- Grid row -->
    </section>
    <!-- Section: Contact v.2 -->


    <!-- Footer -->
    <footer class="page-footer font-small teal pt-4">

        <!-- Footer Elements -->
        <div class="container">

            <!-- Social buttons -->
            <ul class="list-unstyled list-inline text-center">
                <li class="list-inline-item">
                    <a class="btn-floating btn-fb mx-1">
                        <i class="fa fa-facebook-square"> </i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a class="btn-floating btn-tw mx-1">
                        <i class="fa fa-twitter"> </i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a class="btn-floating btn-gplus mx-1">
                        <i class="fa fa-pinterest"> </i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a class="btn-floating btn-li mx-1">
                        <i class="fa fa-linkedin"> </i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a class="btn-floating btn-dribbble mx-1">
                        <i class="fa fa-dribbble"> </i>
                    </a>
                </li>
            </ul>
            <!-- Social buttons -->

        </div>
        <!-- Footer Elements -->

        <!-- Copyright -->
        <div class="footer-copyright text-center py-3"> © 2018 Copyright: Online Voting System designed By Booust Team D.</div>

        <!-- Copyright -->

    </footer>
    <!-- Footer -->


    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>
    <!-- Validation scripts -->
    <script type="text/javascript" src="js/script.js"></script>

</body>

</html>