<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>About Us</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('/ncss/bootstrap.min.css')}}" rel="stylesheet" >
    <!-- Material Design Bootstrap -->
    <link href="{{asset('/ncss/mdb.min.css')}}" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="{{asset('/ncss/style.css')}}" rel="stylesheet">
  
</head>

<body>

    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark teal mb-5 fixed-top animated bounceIn">

        <!-- Navbar brand -->
        <a class="navbar-brand" href="index.html">
            <img class="img-fluid" height="30" width="50" src="img/logo.png">
        </a>

        <!-- Collapse button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
            aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Collapsible content -->
        <div class="collapse navbar-collapse" id="navbar">

            <!-- Links -->
              <ul class="navbar-nav mr-auto nav-spacing">
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/')}}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/contact')}}">Contact</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/about')}}">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Demo</a>
                </li>

            </ul>
            <!-- Links -->

          <div class="form-inline">
                <a href="{!! url('/login')!!}"><button class="btn btn-outline-white btn-md my-2 my-sm-0 ml-3" type="submit">Login</button></a>
                <a href="{!! url('/register')!!}"><button class="btn btn-outline-white btn-md my-2 my-sm-0 ml-3" type="submit">Register</button></a>
            </div>
        </div>
        <!-- Collapsible content -->

    </nav>
    <!--/.Navbar-->

    <!-- Section: Team v.2 -->
    <section class="team-section text-center pt-5 my-5">

        <!-- Section heading -->
        <h2 class="h1-responsive font-weight-bold my-5">Our amazing team</h2>
        <!-- Section description -->
        <p class="grey-text w-responsive mx-auto mb-5">Meet the amazing collaborators who dedicated their time, resources and energy and they also stopped at nothing to makes this Awesome online voting system possible</p>

        <!-- Grid row -->
        <div class="row text-center animated slideInDown">

            <!-- Grid column -->
            <div class="col-md-4">
                <div class="avatar mx-auto">
                    <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(31).jpg" class="rounded z-depth-1-half"
                        alt="Sample avatar">
                </div>
                <h4 class="font-weight-bold dark-grey-text my-4">Omosehin Abayomi</h4>
                <h6 class="text-uppercase grey-text mb-3"><strong>Front-End Web Developer</strong></h6>
                <!-- GitHub-->
                <a type="button" class="btn-floating btn-md mx-1 mb-0 btn-git">
                    <i class="fa fa-github"></i>
                </a>
                <!-- Twitter -->
                <a type="button" class="btn-floating btn-md mx-1 mb-0 btn-tw">
                    <i class="fa fa-twitter "></i>
                </a>
                <!--Facebook -->
                <a type="button" class="btn-floating btn-md mx-1 mb-0 btn-fb">
                    <i class="fa fa-facebook-square "></i>
                </a>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-4">
                <div class="avatar mx-auto">
                    <img src="img/niyi.jpg" class="rounded z-depth-1-half"
                        alt="Sample avatar">
                </div>
                <h4 class="font-weight-bold dark-grey-text my-4">Adegbola Adeniyi</h4>
                <h6 class="text-uppercase grey-text mb-3"><strong>Front-End Web Developer</strong></h6>
                <!-- GitHub-->
                <a type="button" class="btn-floating btn-md mx-1 mb-0 btn-git">
                    <i class="fa fa-github"></i>
                </a>
                <!-- Twitter -->
                <a type="button" class="btn-floating btn-md mx-1 mb-0 btn-tw">
                    <i class="fa fa-twitter "></i>
                </a>
                <!--Facebook -->
                <a type="button" class="btn-floating btn-md mx-1 mb-0 btn-fb">
                    <i class="fa fa-facebook-square "></i>
                </a>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-4">
                <div class="avatar mx-auto">
                    <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(31).jpg" class="rounded z-depth-1-half"
                        alt="Sample avatar">
                </div>
                <h4 class="font-weight-bold dark-grey-text my-4">Olasunkanmi Fatima </h4>
                <h6 class="text-uppercase grey-text mb-3"><strong>Front-End Web Developer</strong></h6>
                <!-- GitHub-->
                <a type="button" class="btn-floating btn-md mx-1 mb-0 btn-git">
                    <i class="fa fa-github"></i>
                </a>
                <!-- Twitter -->
                <a type="button" class="btn-floating btn-md mx-1 mb-0 btn-tw">
                    <i class="fa fa-twitter "></i>
                </a>
                <!--Facebook -->
                <a type="button" class="btn-floating btn-md mx-1 mb-0 btn-fb">
                    <i class="fa fa-facebook-square "></i>
                </a>
            </div>
        </div>
        <!-- Grid column -->

        <div class="row text-center mt-5 animated slideInUp">

            <!-- Grid column -->
            <div class="col-md-4">
                <div class="avatar mx-auto">
                    <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(31).jpg" class="rounded z-depth-1-half"
                        alt="Sample avatar">
                </div>
                <h4 class="font-weight-bold dark-grey-text my-4">Sarah Melyah</h4>
                <h6 class="text-uppercase grey-text mb-3"><strong>Back-End Web Developer</strong></h6>
                <!-- GitHub-->
                <a type="button" class="btn-floating btn-md mx-1 mb-0 btn-git">
                    <i class="fa fa-github"></i>
                </a>
                <!-- Twitter -->
                <a type="button" class="btn-floating btn-md mx-1 mb-0 btn-tw">
                    <i class="fa fa-twitter "></i>
                </a>
                <!--Facebook -->
                <a type="button" class="btn-floating btn-md mx-1 mb-0 btn-fb">
                    <i class="fa fa-facebook-square "></i>
                </a>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-4">
                <div class="avatar mx-auto">
                    <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(31).jpg" class="rounded z-depth-1-half"
                        alt="Sample avatar">
                </div>
                <h4 class="font-weight-bold dark-grey-text my-4">Princewill Okeugo</h4>
                <h6 class="text-uppercase grey-text mb-3"><strong>Back-End Web Developer</strong></h6>
                <!-- GitHub-->
                <a type="button" class="btn-floating btn-md mx-1 mb-0 btn-git">
                    <i class="fa fa-github"></i>
                </a>
                <!-- Twitter -->
                <a type="button" class="btn-floating btn-md mx-1 mb-0 btn-tw">
                    <i class="fa fa-twitter "></i>
                </a>
                <!--Facebook -->
                <a type="button" class="btn-floating btn-md mx-1 mb-0 btn-fb">
                    <i class="fa fa-facebook-square "></i>
                </a>
            </div>
            <!-- Grid column -->

            <!-- Grid column -->
            <div class="col-md-4">
                <div class="avatar mx-auto">
                    <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(31).jpg" class="rounded z-depth-1-half"
                        alt="Sample avatar">
                </div>
                <h4 class="font-weight-bold dark-grey-text my-4">Chetam Okafor</h4>
                <h6 class="text-uppercase grey-text mb-3"><strong>Back-End Web Developer</strong></h6>
                <!-- GitHub-->
                <a type="button" class="btn-floating btn-md mx-1 mb-0 btn-git">
                    <i class="fa fa-github"></i>
                </a>
                <!-- Twitter -->
                <a type="button" class="btn-floating btn-md mx-1 mb-0 btn-tw">
                    <i class="fa fa-twitter "></i>
                </a>
                <!--Facebook -->
                <a type="button" class="btn-floating btn-md mx-1 mb-0 btn-fb">
                    <i class="fa fa-facebook-square "></i>
                </a>
            </div>
            <!-- Grid column -->
        </div>

    </section>
    <!-- Section: Team v.2 -->
    <!-- Footer -->
    <footer class="page-footer font-small teal pt-4">

    <!-- Footer Elements -->
    <div class="container">

      <!-- Social buttons -->
      <ul class="list-unstyled list-inline text-center">
        <li class="list-inline-item">
          <a class="btn-floating btn-fb mx-1">
            <i class="fa fa-facebook-square"> </i>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="btn-floating btn-tw mx-1">
            <i class="fa fa-twitter"> </i>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="btn-floating btn-gplus mx-1">
            <i class="fa fa-pinterest"> </i>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="btn-floating btn-li mx-1">
            <i class="fa fa-linkedin"> </i>
          </a>
        </li>
        <li class="list-inline-item">
          <a class="btn-floating btn-dribbble mx-1">
            <i class="fa fa-dribbble"> </i>
          </a>
        </li>
      </ul>
      <!-- Social buttons -->

    </div>
    <!-- Footer Elements -->

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3"> © 2018 Copyright: Online Voting System designed By Booust Team D.</div>
    <!-- Copyright -->

  </footer>
  <!-- Footer -->

    <!-- SCRIPTS -->
    
    <!-- JQuery -->
    <script type="text/javascript" src="{{asset('/njs/jquery-3.3.1.min.js')}}"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="{{asset('/njs/popper.min.js')}}"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="{{asset('/njs/bootstrap.min.js')}}"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="{{asset('/njs/mdb.min.js')}}"></script>
    
</body>

</html>