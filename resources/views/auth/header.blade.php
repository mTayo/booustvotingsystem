<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Booust Voting System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('/ncss/login.css')}}" >
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
        crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('/ncss/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="{{asset('/ncss/mdb.min.css')}}" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="{{asset('/ncss/style.css')}}" rel="stylesheet">


</head>

<body>
    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark teal fixed-top">

        <!-- Navbar brand -->
        <a class="navbar-brand" href="{{url('/')}}">
            <img class="img-fluid" height="30" width="50" src="img/logo.png">
        </a>

        <!-- Collapse button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
            aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Collapsible content -->
        <div class="collapse navbar-collapse" id="navbar">

            <!-- Links -->
            <ul class="navbar-nav mr-auto nav-spacing">
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/')}}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/contact')}}">Contact</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/about')}}">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Demo</a>
                </li>

            </ul>
            <!-- Links -->

            <div class="form-inline">
                <!-- <button class="btn btn-outline-white btn-md my-2 my-sm-0 ml-3" type="submit">Login</button> -->
               <!-- <button class="btn btn-outline-white btn-md my-2 my-sm-0 ml-3" type="submit">Register</button>-->
            </div>
        </div>
        <!-- Collapsible content -->

    </nav>
    <!--/.Navbar-->