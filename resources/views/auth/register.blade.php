@extends('auth.layout')
@section('content')

    <div class="color-overlay">
        <div class="container">
 			<div class="row reg">
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 col-xl-5 right">
                    <h5><b>Booust E-Voting System</b></h5>
                    <p>As a registered User, Click on Login button<br/>
                    As a new user, click on Register button</p>
                </div>
                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 col-xl-7 left">
                    <h2>Login/Register Here</h2>
                    <form method="post" action="{{url('user-registration')}}">
                    	@csrf
                        <div class="form-group">
                            <label>Fullname</label>
                            <input type="text" class="form-control" name="fullname" value="{{old('fullname')}}" placeholder="Full name">
                            @if($errors->has('fullname'))
                            <span class="form-text text-danger">{{$errors->first('fullname')}}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" name="email" value="{{old('email')}}"   placeholder="Email">
                            @if($errors->has('email'))
                            <span class="form-text text-danger">{{$errors->first('email')}}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Occupation</label>
                            <input type="text" class="form-control" name="occupation" value="{{old('occupation')}}"  placeholder="Occupation">
                            @if($errors->has('occupation'))
                            <span class="form-text text-danger">{{$errors->first('occupation')}}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" value="{{old('password')}}"  placeholder="Password">
                            @if($errors->has('password'))
                            <span class="form-text text-danger">{{$errors->first('password')}}</span>
                            @endif
                        </div>
                         <div class="form-group">
                            <label>Confirm Password</label>
                            <input type="password" class="form-control" name="confirm_password" value="{{old('confirm_password')}}"  placeholder="Confirm-Password">
                            @if($errors->has('confirm_password'))
                            <span class="form-text text-danger">{{$errors->first('confirm_password')}}</span>
                            @endif
                        </div>
                        <div class="form-check">
						  <input class="form-check-input" type="radio" name="register_as" value="voter" checked>
						  <label class="form-check-label" >
						    Register as voter
						  </label>
						</div>
						<div class="form-check">
						  <input class="form-check-input" type="radio" name="register_as" value="contestant">
						  <label class="form-check-label">
						   	Register as contestant
						  </label>
						</div>
						@if($errors->has('register_as'))
                            <span class="form-text text-danger">{{$errors->first('register_as')}}</span>
                        @endif
                        <div class="form-inline">
                        <button type="submit" class="btn btn-primary mb-2">Register</button>
                       <!--  <button type="submit" class="btn btn-success mb-2">Register</button> --><span class="mx-sm-3 mb-2">Already have an account?</span><a href="{!! url('/login')!!}" role="button"  class="btn-btn info">Login</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection