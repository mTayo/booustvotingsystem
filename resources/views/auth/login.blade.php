<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Booust Voting System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="{{asset('/ncss/login.css')}}" >
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB"
        crossorigin="anonymous" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('/ncss/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="{{asset('/ncss/mdb.min.css')}}" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="{{asset('/ncss/style.css')}}" rel="stylesheet">


</head>

<body>
    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark teal fixed-top">

        <!-- Navbar brand -->
        <a class="navbar-brand" href="{{url('/')}}">
            <img class="img-fluid" height="30" width="50" src="img/logo.png">
        </a>

        <!-- Collapse button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar"
            aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Collapsible content -->
        <div class="collapse navbar-collapse" id="navbar">

            <!-- Links -->
            <ul class="navbar-nav mr-auto nav-spacing">
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/')}}">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/contact')}}">Contact</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('/about')}}">About</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Demo</a>
                </li>

            </ul>
            <!-- Links -->

            <div class="form-inline">
                <!-- <button class="btn btn-outline-white btn-md my-2 my-sm-0 ml-3" type="submit">Login</button> -->
               <!-- <button class="btn btn-outline-white btn-md my-2 my-sm-0 ml-3" type="submit">Register</button>-->
            </div>
        </div>
        <!-- Collapsible content -->

    </nav>
    <!--/.Navbar-->

    <div class="color-overlay pt-5 mt-5">


     {{--display flash data--}}
                @if(session('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        {{session('success')}}
                        <button type="button" class="close" data-dismiss="alert">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                @if(session('error'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{session('error')}}
                        <button type="button" class="close" data-dismiss="alert">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endif
                
        <div class="container mb-5">
            <div class="row">
                <div class="col-xs-5 col-sm-5 col-md-5 col-lg-5 col-xl-5 right">
                    <h5><b>Booust E-Voting System</b></h5>
                    <p>As a registered User, Click on Login button</br>
                        As a new user, click on Register button</p>
                </div>
                <div class="col-xs-7 col-sm-7 col-md-7 col-lg-7 col-xl-7 left" >
                    <h2>Login/Register Here</h2>
                     <form method="post" action="{{url('/verify-login')}}">
                        @csrf
                       <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="email" placeholder="Your Email">
                        </div>
                         @if($errors->has('email'))
                         <span class="form-text text-danger">{{$errors->first('email')}}</span>
                         @endif
                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password" placeholder="Password">
                        </div>
                        @if($errors->has('password'))
                        <span class="form-text text-danger">{{$errors->first('password')}}</span>
                         @endif
                        <div class="form-inline">
                            <button type="submit" class="btn btn-teal mb-2">Login</button>
                            <span class="mx-sm-3 mb-2">Don't have an account?</span><a href="{{url('/register')}}"> <button type="button" class="btn btn-teal mb-2">Register</button></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
     <footer class="page-footer font-small teal">

        <!-- Footer Elements -->
        <div class="container">

            <!-- Social buttons -->
            <ul class="list-unstyled list-inline text-center">
                <li class="list-inline-item">
                    <a class="btn-floating btn-fb mx-1">
                        <i class="fa fa-facebook-square"> </i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a class="btn-floating btn-tw mx-1">
                        <i class="fa fa-twitter"> </i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a class="btn-floating btn-gplus mx-1">
                        <i class="fa fa-pinterest"> </i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a class="btn-floating btn-li mx-1">
                        <i class="fa fa-linkedin"> </i>
                    </a>
                </li>
                <li class="list-inline-item">
                    <a class="btn-floating btn-dribbble mx-1">
                        <i class="fa fa-dribbble"> </i>
                    </a>
                </li>
            </ul>
            <!-- Social buttons -->

        </div>
        <!-- Footer Elements -->

        <!-- Copyright -->
        <div class="footer-copyright text-center py-3"> © 2018 Copyright: Online Voting System designed By Booust Team
            D.</div>

        <!-- Copyright -->

    </footer>
    <!-- Footer -->


    <!-- SCRIPTS -->
    
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <!-- Popper JS -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.0/umd/popper.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.0/js/bootstrap.min.js"></script>
</body>

</html>